package com.projeto.api.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.projeto.api.model.Cliente;
import com.projeto.api.repositories.ClienteRepository;

@Service
public class ClienteService {
	
	@Autowired
	private ClienteRepository clienteRespository;
	
	public void createCliente (Cliente cliente) {
		clienteRespository.save(cliente);
	}
	
	public List<Cliente> getClientes () {
		return clienteRespository.findAll();
	}
	
	public Cliente getClienteById (Long id) {
		Optional<Cliente> optional = clienteRespository.findById(id);
		if (optional.isPresent()) {
			return optional.get();			
		} else {
			return null;
		}
	}
	
	public void editCliente (Cliente cliente) {
		clienteRespository.save(cliente);
	}
	
	public void deleteCliente (Long id) {
		clienteRespository.deleteById(id);
	}
	
	public Cliente getClienteByCpf (String cpf) {
		return clienteRespository.findByCpf(cpf);
	}
	
	public Cliente getClienteByCnpj (String cnpj) {
		return clienteRespository.findByCnpj(cnpj);
	}

}
