package com.projeto.api.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.projeto.api.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {

	Cliente findByCnpj(String cnpj);
	Cliente findByCpf(String cpf);
}
