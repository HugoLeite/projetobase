package com.projeto.api.controllers;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.api.dto.ClienteDto;
import com.projeto.api.model.Cliente;
import com.projeto.api.responses.Response;
import com.projeto.api.services.ClienteService;

@RestController
@RequestMapping("/cliente")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping
	public ResponseEntity<Response<ClienteDto>> cadastrar (@Valid @RequestBody ClienteDto clienteDto,
			BindingResult result) {
		Response<ClienteDto> response = new Response<>();
		
		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErros().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		if (StringUtils.isEmpty(clienteDto.getCnpj()) && StringUtils.isEmpty(clienteDto.getCpf())) {
			response.getErros().add("Informe o CPF ou o CNPJ");
			return ResponseEntity.badRequest().body(response);
		}
		
		Cliente cliente = convertClienteDtoToCliente(clienteDto);
		clienteService.createCliente(cliente);
		response.setData(convertClientToClientDto(cliente));
		
		return ResponseEntity.ok(response);
	}
	
	@GetMapping
	public ResponseEntity<Response<List<ClienteDto>>> getClientes () {
		Response<List<ClienteDto>> response = new Response<>();
		response.setData(new ArrayList<ClienteDto>());
		List<Cliente> clientes = clienteService.getClientes();
		clientes.forEach(cli -> response.getData().add(convertClientToClientDto(cli)));
		return ResponseEntity.ok(response);
	}
	
	@GetMapping("/{id}")
	public ResponseEntity<Response<ClienteDto>> getCliente (@PathVariable("id") Long id) {
		Response<ClienteDto> response = new Response<>();
		Cliente cliente = clienteService.getClienteById(id);
		
		if (cliente != null) {
			response.setData(convertClientToClientDto(cliente));
		}
		
		return ResponseEntity.ok(response);
	}
	
	@PatchMapping("/{id}")
	public ResponseEntity<Response<ClienteDto>> editar (@PathVariable("id") Long id, @Valid @RequestBody ClienteDto clienteDto,
			BindingResult result) {
		Response<ClienteDto> response = new Response<>();
		
		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErros().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		if (StringUtils.isEmpty(clienteDto.getCnpj()) && StringUtils.isEmpty(clienteDto.getCpf())) {
			response.getErros().add("Informe o CPF ou o CNPJ");
			return ResponseEntity.badRequest().body(response);
		}
		
		clienteDto.setId(id);
		clienteService.editCliente(convertClienteDtoToCliente(clienteDto));
		
		return ResponseEntity.ok(response);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<Response<ClienteDto>> delete (@PathVariable("id") Long id) {
		Response<ClienteDto> response = new Response<>();
		clienteService.deleteCliente(id);
		
		return ResponseEntity.ok(response);
	}
	
	private ClienteDto convertClientToClientDto(Cliente cliente) {
		ClienteDto clienteDto = new ClienteDto();
		clienteDto.setId(cliente.getId());
		clienteDto.setCep(cliente.getCep());
		clienteDto.setClassificacao(cliente.getClassificacao());
		clienteDto.setCnpj(cliente.getCnpj());
		clienteDto.setCpf(cliente.getCpf());
		clienteDto.setEmail(cliente.getEmail());
		clienteDto.setNome(cliente.getNome());
		clienteDto.setPontos(cliente.getPontos());
		clienteDto.setRazaoSocial(cliente.getRazaoSocial());
		clienteDto.setTelefone(cliente.getTelefone());
		return clienteDto;
	}
	
	private Cliente convertClienteDtoToCliente(ClienteDto clienteDto) {
		Cliente cliente = new Cliente();
		cliente.setId(clienteDto.getId());
		cliente.setCep(clienteDto.getCep());
		cliente.setClassificacao(clienteDto.getClassificacao());
		cliente.setCnpj(clienteDto.getCnpj());
		cliente.setCpf(clienteDto.getCpf());
		cliente.setDataCriacao(new Date());
		cliente.setEmail(clienteDto.getEmail());
		cliente.setNome(clienteDto.getNome());
		cliente.setPontos(clienteDto.getPontos());
		cliente.setRazaoSocial(clienteDto.getRazaoSocial());
		cliente.setTelefone(clienteDto.getTelefone());
		return cliente;
	}

}
