package com.projeto.api.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.api.dto.DescontoRequestDto;
import com.projeto.api.dto.DescontoResponseDto;
import com.projeto.api.enums.ClassificacaoEnum;
import com.projeto.api.model.Cliente;
import com.projeto.api.responses.Response;
import com.projeto.api.services.ClienteService;

@RestController
@RequestMapping("/desconto")
public class DescontoController {
	
	@Autowired
	private ClienteService clienteService;
	
	@PostMapping
	public ResponseEntity<Response<DescontoResponseDto>> calcular (@Valid @RequestBody DescontoRequestDto descontoDto,
			BindingResult result) {
		Response<DescontoResponseDto> response = new Response<>();
		response.setData(new DescontoResponseDto());
		
		if (result.hasErrors()) {
			result.getAllErrors().forEach(error -> response.getErros().add(error.getDefaultMessage()));
			return ResponseEntity.badRequest().body(response);
		}
		
		if (StringUtils.isEmpty(descontoDto.getCnpj()) && StringUtils.isEmpty(descontoDto.getCpf())) {
			response.getErros().add("Informe o CPF ou o CNPJ");
			return ResponseEntity.badRequest().body(response);
		}
		
		Cliente cliente;
		
		if (!StringUtils.isEmpty(descontoDto.getCpf())) {
			cliente = clienteService.getClienteByCpf(descontoDto.getCpf());
		} else {
			cliente = clienteService.getClienteByCnpj(descontoDto.getCnpj());
		}
		
		if (cliente != null) {
			if (cliente.getClassificacao().equals(ClassificacaoEnum.INATIVO)) {
				response.getData().setValorDesconto(descontoDto.getValor());
			} else if (cliente.getClassificacao().equals(ClassificacaoEnum.ATIVO)) {
				if (cliente.getPontos() <= 300) {
					response.getData().setValorDesconto(descontoDto.getValor() - descontoDto.getValor() * 0.03);
				} else if (cliente.getPontos() <= 1000) {
					response.getData().setValorDesconto(descontoDto.getValor() - descontoDto.getValor() * 0.05);
				} else {
					response.getData().setValorDesconto(descontoDto.getValor() - descontoDto.getValor() * 0.1);
				}
			} else {
				if (cliente.getPontos() <= 300) {
					response.getData().setValorDesconto(descontoDto.getValor() - descontoDto.getValor() * 0.06);
				} else if (cliente.getPontos() <= 1000) {
					response.getData().setValorDesconto(descontoDto.getValor() - descontoDto.getValor() * 0.1);
				} else {
					response.getData().setValorDesconto(descontoDto.getValor() - descontoDto.getValor() * 0.2);
				}
			}
		}
		
		return ResponseEntity.ok(response);
	}

}
