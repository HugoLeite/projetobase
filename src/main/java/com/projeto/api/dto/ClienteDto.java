package com.projeto.api.dto;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

import com.projeto.api.enums.ClassificacaoEnum;

public class ClienteDto {
	private Long id;
	@NotEmpty(message = "Nome obrigatório")
	@Size(max=100, message="Nome deve ter no máximo 100 caracteres")
	private String nome;
	@Size(max=100, message="Razão Social deve ter no máximo 100 caracteres")
	private String razaoSocial;
	@CPF(message = "CPF inválido")
	private String cpf;
	@CNPJ(message = "CNPJ inválido")
	private String cnpj;
	@NotEmpty(message = "Informe o cep")
	private String cep;
	@NotEmpty(message = "E-mail obrigatório")
	@Email(message = "E-mail inválido")
	private String email;
	@Enumerated(EnumType.STRING)
	private ClassificacaoEnum classificacao;
	@NotNull(message = "Informe os pontos")
	private Integer pontos;
	@NotEmpty(message = "Informe o telefone")
	private String telefone;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getRazaoSocial() {
		return razaoSocial;
	}
	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getCep() {
		return cep;
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public ClassificacaoEnum getClassificacao() {
		return classificacao;
	}
	public void setClassificacao(ClassificacaoEnum classificacao) {
		this.classificacao = classificacao;
	}
	public Integer getPontos() {
		return pontos;
	}
	public void setPontos(Integer pontos) {
		this.pontos = pontos;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
}
