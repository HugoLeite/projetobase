package com.projeto.api.dto;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.br.CNPJ;
import org.hibernate.validator.constraints.br.CPF;

public class DescontoRequestDto {
	
	@CPF(message = "CPF inválido")
	private String cpf;
	@CNPJ(message = "CNPJ inválido")
	private String cnpj;
	@NotNull(message = "Informe o valor do desconto")
	private Double valor;
	
	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}

}
