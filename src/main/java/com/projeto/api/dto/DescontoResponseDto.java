package com.projeto.api.dto;

public class DescontoResponseDto {
	
	private Double valorDesconto;

	public Double getValorDesconto() {
		return valorDesconto;
	}

	public void setValorDesconto(Double valorDesconto) {
		this.valorDesconto = valorDesconto;
	}
}
